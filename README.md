# Spectral - the main theme of Spectra.Video

Spectral is a retro-style theme in the vein of mid-aughts web applications, and
leans more towards gradients and highlights than the flat design variants that
came later.

Originally, this theme started as some CSS overrides for the instance. It is in
the process of being converted into a proper theme, so that our home instance
can offer several different themes for users.

This theme is designed for PeerTube v 3.3.0 and up! Please submit an issue if
you see any bugs or have ideas on how to make the theme better.
